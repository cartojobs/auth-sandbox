import { AuthFacebookService } from './../../common/services/auth-facebook.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-facebook-like',
  templateUrl: './facebook-like.component.html',
  styleUrls: ['./facebook-like.component.scss']
})
export class FacebookLikeComponent {

  constructor(private authFacebookService: AuthFacebookService) {

  }

}
