import { environment } from './../../../../environments/environment';
import { AuthService } from './../../../common/services/auth.service';
import { LocalStorageService } from './../../../common/services/local-storage.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jwt',
  templateUrl: './jwt.component.html',
  styleUrls: ['./jwt.component.scss']
})
export class JwtComponent {

  jwt;
  chuck;
  loginForm;

  constructor(private http: HttpClient, private localStorageService: LocalStorageService, private authService: AuthService) {
    this.loginForm = new FormGroup({
      username: new FormControl('admin', [Validators.required]),
      password: new FormControl('admin', [Validators.required])
    });
    this.jwt = this.localStorageService.jwt;
  }

  callLogin(username: string, password: string) {
    this.authService.login(username, password).subscribe(
      () => this.jwt = this.localStorageService.jwt
    );
  }

  chuckSentenses() {
    return this.http.get('http://localhost:3001/api/protected/random-quote');
  }

  callChuckSentenses() {
    this.chuckSentenses().subscribe(res => this.chuck = res);
  }

  isProd(): boolean {
    return environment.production;
  }

}
