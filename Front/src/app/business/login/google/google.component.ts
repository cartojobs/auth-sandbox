import { environment } from './../../../../environments/environment';
import { Component, OnInit, NgZone } from '@angular/core';
import { Meta } from '@angular/platform-browser';
declare const gapi: any;

@Component({
  selector: 'app-google',
  templateUrl: './google.component.html',
  styleUrls: ['./google.component.scss']
})
export class GoogleComponent {

  profile;

  constructor(private zone: NgZone, private Meta:Meta) {
    this.Meta.addTag({ name: 'google-signin-client_id', content: environment.googleSigninClientId });
  }

  ngAfterViewInit() {
    gapi.signin2.render('google-signin', {
      'scope': 'profile email',
      'width': 240,
      'height': 50,
      'longtitle': true,
      'theme': 'light',
      'onsuccess': param => this.onSignIn(param)
    });
  }

  public onSignIn(googleUser) {
    const profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    this.zone.run(() => {
      //Here add the code to force the value update
      this.profile = profile; // This value will be force updated
    });
  }

}
