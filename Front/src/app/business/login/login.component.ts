import { AuthService } from './../../common/services/auth.service';
import { LocalStorageService } from './../../common/services/local-storage.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  _active = 'google';

  get active(): string {
    return this._active;
  }

  set active(active: string) {
    this._active = active;
  }

}
