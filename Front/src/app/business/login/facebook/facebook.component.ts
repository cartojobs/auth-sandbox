import { AuthFacebookService } from './../../../common/services/auth-facebook.service';
import { Component, OnInit } from '@angular/core';
declare var window: any;
declare var FB: any;

@Component({
  selector: 'app-facebook',
  templateUrl: './facebook.component.html',
  styleUrls: ['./facebook.component.scss']
})
export class FacebookComponent {

  constructor(private authFacebookService: AuthFacebookService) {

  }

}
