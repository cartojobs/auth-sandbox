import { FacebookComponent } from './business/login/facebook/facebook.component';
import { ApiInterceptorService } from './common/services/api-interceptor.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './business/login/login.component';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WebStorageModule } from 'ngx-store';
import { FacebookLikeComponent } from './business/facebook/facebook-like.component';
import { JwtComponent } from './business/login/jwt/jwt.component';
import { GoogleComponent } from './business/login/google/google.component';
import { DividerComponent } from './common/components/divider/divider.component';
// import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FacebookLikeComponent,
    FacebookComponent,
    JwtComponent,
    GoogleComponent,
    DividerComponent
  ],
  imports: [
    BrowserModule,
    //AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    WebStorageModule
  ],
  providers: [
    ApiInterceptorService,
    {
      provide    : HTTP_INTERCEPTORS,
      multi      : true,
      useExisting: ApiInterceptorService
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
