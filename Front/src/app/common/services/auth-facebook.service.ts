import { Injectable } from '@angular/core';

declare var window: any;
declare var FB: any;

@Injectable({
  providedIn: 'root'
})
export class AuthFacebookService {

  appId = 265047481088830;
  version = 'v3.2';

  constructor() {
    this.loadFbSDK(document, 'script', 'facebook-jssdk');

    window.fbAsyncInit = () => {
      console.log("fbasyncinit");
      FB.init({
        appId: this.appId,
        autoLogAppEvents: true,
        xfbml: true,
        version: this.version
      });
      FB.AppEvents.logPageView();
      // This is where we do most of our code dealing with the FB variable like adding an observer to check when the user signs in
      // ** ADD CODE TO NEXT STEP HERE **
      FB.getLoginStatus((response) => {
        //statusChangeCallback(response);
        console.log(response);
      });
      // Examples : https://developers.facebook.com/docs/javascript/examples
      /*FB.ui({
        method: 'share',
        href: 'https://developers.facebook.com/docs/'
      }, function(response){});*/
    };
    // ----------
  }

  loadFbSDK(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  }

}
