import { Injectable } from '@angular/core';
import { CookieStorage, LocalStorage, SessionStorage } from 'ngx-store';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  @LocalStorage()
  jwt;

  constructor() { }
}
