import { LocalStorageService } from './local-storage.service';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, ReplaySubject, Subject, throwError as _throw } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiInterceptorService implements HttpInterceptor {

  constructor(private localStorageService: LocalStorageService) { }

  /**
   * L'intercepteur permet de mettre une base d'URL sur toute les requêtes transmise a l'exception des requ$ete commencant par ~.
   * Il permet également d'intercepté toute erreur pour la convertir en objet ServerError avec la traduction de l'erreur associé.
   * @param {HttpRequest<any>} request
   * @param {HttpHandler} next
   * @returns {Observable<HttpEvent<any>>}
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const jwt = this.localStorageService.jwt;
    if (jwt) {
      request = request.clone({
        responseType: 'text',
        setHeaders: {
          Authorization: `Bearer ${jwt}`,
        },
      });
    }

    return next.handle(request);
  }

}
