import { LocalStorageService } from './local-storage.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private localStorageService: LocalStorageService) { }

  login(username:string, password:string ) {
    return this.http.post('http://localhost:3001/sessions/create', {username, password}).pipe(
      tap(res => this.localStorageService.jwt = res['access_token'])
    );
  }

}
